TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Board/src/coordinate.cpp \
    Board/src/direction.cpp \
    Tron/src/lightcycle.cpp \
    Tron/src/tronbattle.cpp \
    Board/src/square.cpp \
    Tron/src/tronbattlesquare.cpp \
    Tron/src/computerbase.cpp \
    Tron/src/movementestimation.cpp \
    Tron/src/computer2.cpp \
    Tron/src/computer0.cpp \
    Tron/src/noreturnzone.cpp

HEADERS += \
    Tron/lightcycle.h \
    Board/Board \
    Board/coordinate.h \
    Board/direction.h \
    Tron/tronbattle.h \
    Tron/Tron \
    Board/gameboard.h \
    Board/square.h \
    Tron/tronbattlesquare.h \
    Tron/computerbase.h \
    Tron/movementestimation.h \
    Tron/computer2.h \
    Tron/computer0.h \
    Tron/noreturnzone.h

