# README #

### What is this repository for? ###

* This repository is a simple AI for a multi-player tron battle game.
* I created this AI for a [codingame.com contest](https://www.codingame.com/leaderboards/challenge/20/global). (my pseudo is MonsieurV)

### How do I get set up? ###

* The only dependency is the standard C++ libraries.
* In order to test it, you need a soft that manage the tron battle. You can check the one i make using QT [here](https://bitbucket.org/monsieurV/tronbattle) (linux user only, sorry).